#include <json-c/json.h>
#include <string.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "cpu.h"


//function that return a json file containing all the  cpu_parametres in a json file  //

void collect_cpu_values_general() {
  FILE *fin;
  cpu_measurement_t  cpu ;
  char cpu_user   [6]; 
  char cpu_nice   [6]; 
  char cpu_system [6];
  char cpu_iowait[6];
  char cpu_irq[6];
  char cpu_softirq[6];
  char cpu_steal_time[6];
  char cpu_guest_system[6];
  char var[90] ;
  char M[6] ;
  char N[6] ;
  char time[20] ;
  char cpu_idle[10];


    fin=popen(" mpstat " ,"r") ;
    

    while (fgets( var,sizeof(var),fin)!=NULL )
     {
        if (strstr(var,"CPU")) 
            fgets( var,sizeof(var),fin); 
           // printf("%s", var) ;

            sscanf(var,  "%s     %s   %s   %s    %s %s    %s   %s  %s  %s  %s   %s ",cpu_idle,N,cpu_user,cpu_nice,cpu_system,cpu_iowait,cpu_irq,cpu_softirq,cpu_steal_time,cpu_guest_system,M,cpu_idle) ;
            
            //printf( "%s     %s   %s   %s    %s %s    %s   %s  %s  %s  %s   %s \n ",time,N,cpu_user,cpu_nice,cpu_system,cpu_iowait,cpu_irq,cpu_softirq,cpu_steal_time,cpu_guest_system,M,cpu_idle) ;
    
     
    cpu.cpu_user = cpu_user ;
    cpu.cpu_nice = cpu_nice ;
    cpu.cpu_system = cpu_system ;
    cpu.cpu_idle   = cpu_idle ;
    cpu.cpu_iowait = cpu_iowait ;
    cpu.cpu_steal_time= cpu_steal_time;
    cpu.cpu_guest_system=cpu_guest_system ;
    cpu.cpu_irq=cpu_irq ;
    cpu.cpu_softirq=cpu_softirq ; 
     }
    
    
     
    json_object * jobj = json_object_new_object();
    json_object *jsrting = json_object_new_string(cpu.cpu_user);
    json_object *jstring1 = json_object_new_string(cpu.cpu_nice);
    json_object *jstring2 = json_object_new_string(cpu.cpu_system);
    json_object *jstring3 = json_object_new_string(cpu.cpu_idle);
    json_object *jstring4 = json_object_new_string(cpu.cpu_iowait);
    json_object *jstring5 = json_object_new_string(cpu.cpu_steal_time);
    json_object *jstring6 = json_object_new_string(cpu.cpu_guest_system);
    json_object *jstring7 = json_object_new_string(cpu.cpu_irq);
    json_object *jstring8 = json_object_new_string(cpu.cpu_softirq);

    json_object_object_add(jobj,"cpu_user", jsrting);
    json_object_object_add(jobj,"cpu_nice", jstring1);
    json_object_object_add(jobj,"cpu_system", jstring2);
    json_object_object_add(jobj,"cpu_idle", jstring3);
    json_object_object_add(jobj,"cpu_iowait", jstring4);
    json_object_object_add(jobj,"cpu_steal_time", jstring5);
    json_object_object_add(jobj,"cpu_guest_system", jstring6);
    json_object_object_add(jobj,"cpu_irq", jstring7);
    json_object_object_add(jobj,"cpu_softirq", jstring8);
    
    printf ("cpu general measurements : %sn",json_object_to_json_string(jobj));

    }

    
// a function that return a json file contain all the parameteres related par process 
    void collect_cpu_values_per_process()
    { 
    json_object * jobj = json_object_new_object();// opening a json file to content all things 
    cpup_measurement_t cpup ={};
    int i = 0 ;
    char var[100];
    char buf[30];
    char rank[3];
    char pid[6];
    char cmd[30];
    char process_percentage[8];
    const char* result ; 
    
   FILE *fin ; 
   fin=popen(" ps -eo pid,%cpu,comm --sort=-%cpu | head " ,"r") ;
    while (fgets( var,sizeof(var),fin)!=NULL )
     {
        if (strstr(var,"PID")) 
            fgets( var,sizeof(var),fin); 
            sscanf(var,"%s %s %s ",pid,process_percentage,cmd) ;
          
            cpup.pid = pid ;
            cpup.process_percentage = process_percentage ;
            cpup.cmd = cmd ;
         
        //Creating a json array   
        json_object *jarray = json_object_new_array() ;
        json_object *jstring2 = json_object_new_string(cpup.pid);
        json_object *jstring3 = json_object_new_string(cpup.process_percentage);
        json_object *jstring4 = json_object_new_string(cpup.cmd);

        //Adding the above created json strings to the array 
       json_object_array_add(jarray,jstring2);
       json_object_array_add(jarray,jstring3);
       json_object_array_add(jarray,jstring4);

       snprintf(buf,10,"process_%d",i) ; 
       json_object_object_add(jobj,buf, jarray);
       i++;
       
    }
    printf ("cpu general measurements per process : %sn",json_object_to_json_string(jobj)) ;

    }


   int main()
   {
       collect_cpu_values_general() ;
       collect_cpu_values_per_process() ;
       return(0);
   }
 