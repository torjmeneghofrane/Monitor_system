
#include <mosquitto.h>
#include <string.h>
#include<stdio.h>
#include "cpu.h"
#define mqtt_host "localhost"
#define mqtt_port 1883


 struct mosquitto *mosq = NULL;
 char *host = "localhost";
 int port = 1883;
 int keepalive = 60;
 bool clean_session = true;
 char topic[20] = "device/cpu";
 
 
 
 int publish_mqtt(const char * report ){
    int  rc ; /*the return code of the connection response*/


    mosquitto_lib_init();
    /* Create a new client instance.
        * id = NULL -> ask the broker to generate a client id for us
        * clean session = true -> the broker should remove old sessions when we connect
        * obj = NULL -> we aren't passing any of our private data for callbacks
        */
    mosq=mosquitto_new(NULL,clean_session,NULL);
    /* Connect to localhost on port 1883 */
    rc=mosquitto_connect(mosq,"localhost",1883,keepalive);
    /*check the connection is established */
    if (rc!=0)
    {
        printf ("client could not connect to broker ! ERROR code : %d\n" ,rc);
        mosquitto_destroy(mosq);
        return(-1) ;

    }
        printf("we are now connected to the broker !\n");
        mosquitto_publish(mosq, 0, topic, strlen(report), report,1, false);
        
        mosquitto_disconnect(mosq);
        mosquitto_destroy(mosq);

        mosquitto_lib_cleanup();
    return(0);
 }

    int main () 
    {
     
     // no reference to these two functions // 
     collect_cpu_values_general();
     collect_cpu_values_per_process();

     return(0) ;  

     //erreur fel make 
     //src/modulecpu.c:7:10: fatal error: cpu.h: No such file or directory
    7 //| #include "cpu.h"
      //|          ^~~~~~~
      //compilation terminated.
      //make: *** [Makefile:8: cpu] Error 1
  
    }

  
