#include <json-c/json.h>
#include <string.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <mosquitto.h>
#include "cpu.h"

  char getDisksize()
    {
     FILE *fp ;
     char var[90] ;
     static char disksize[5]= ""; 
     char lsblk_cmdline [50];
    
     sprintf(lsblk_cmdline,"%s%s%s%s","lsblk |awk ","' $6 == ","\"disk\""," {print $4}'");
    //printf("%s\n",lsblk_cmdline);
     fp=popen(lsblk_cmdline ,"r") ;
     while (fgets( var,sizeof(var),fp)!=NULL ) {
        sscanf(var,"%[^G]", disksize) ; 
        return(disksize) ;
        //printf ("%s \n" ,disksize);  
        fclose(fp) ;
     
    }
 }  

    char getPourcentageutilzation()
    {   
        FILE *fv ;
        char var[90];
        static char utili[6]= "" ;
        //char iostat_command [50];
        //sprintf()
        fv=popen("iostat -mx | awk '/sda/ { print $NF } ' ","r");
         while (fgets( var,sizeof(var),fv)!=NULL ) {
             sscanf( var,"%s",utili); 
             //printf ("%s",utili) ;}

       // while (fgets (var,sizeof(var),fp)="sda"){
       //     sscanf(var,"%s",utili);
       //     pritnf("%s",utili);
       // }
       return(utili);
       fclose(fv) ;
       
         }
    }
   
 void collect_cpu_values_per_process()
 {   
    json_object * jobj = json_object_new_object();
    int i = 0 ;
    char buf[10] ;
    cpup_measurement_t cpup ={};
    FILE *fin ; 
    char buffer[500];
    char rank[3];
    char pid[6];
    char cmd[30];
    char process_percentage[8];
    const char* result ; 

   fin=popen(" ps -eo pid,cmd,%cpu --sort=-%mem | head " ,"r") ;
   result= strstr(buffer,"PID");
   if (result = NULL) 
    fgets(buffer,500,fin) ;
        while (fgets(buffer,500,fin) != NULL )
        {
         sscanf(buffer,"%s %s  %s",pid,cmd,process_percentage);
         ++i;
        
       cpup.rank = i;
       cpup.pid = pid ;
       cpup.cmd = cmd ;
       cpup.process_percentage = process_percentage ;
       
       //Creating a json array   
       json_object *jarray = json_object_new_array();
       json_object *jstring1 = json_object_new_string("i");
       json_object *jstring2 = json_object_new_string("pid");
       // nom de process selon le pid  
       json_object *jstring3 = json_object_new_string("process_percentage");

      //Adding the above created json strings to the array 
        json_object_array_add(jarray,jstring1);
        json_object_array_add(jarray,jstring2);
        json_object_array_add(jarray,jstring3);
        // snprintf(buf, 12, "pre_%d_suff", i);
        snprintf(buf,10,"process_%d",i) ; 

     /*Form the json object*/

       json_object_object_add(jobj,buf, jarray);

     /*Now printing the json object*/
      printf ("The json object created: %sn",json_object_to_json_string(jobj));

}


       
       

       }
       }


 }
  void collect_RAM_values_per_process()
 {   
    
    FILE *fin ; 
    char buffer[500];
    char rank[3];
    char pid[6];
    char cmd[30];
    char process_percentage[8];

fin=popen(" ps -eo pid,cmd,%cpu --sort=-%mem | head " ,"r") ;

//      PID CMD                         %CPU
//   1739 /usr/bin/gnome-shell         4.5
//  27436 /opt/google/chrome/chrome    0.9
//   1990 /snap/snap-store/518/usr/bi  0.0
//  57135 /opt/google/chrome/chrome - 13.3
//  27839 /usr/share/code/code --type  1.2
//  33478 /opt/google/chrome/chrome -  0.2
//  51856 /usr/bin/totem --gapplicati  0.1
//  56000 /usr/lib/thunderbird/thunde  0.1
//  55574 /opt/google/chrome/chrome -  0.7

   int i=0 ;	 
   while (fgets(buffer,500,fin) != NULL)	
      {
       sscanf(buffer,"%s %s  %s",pid,cmd,process_percentage);
      
      //kamelha
      }     
 }