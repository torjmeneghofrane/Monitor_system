#include <json-c/json.h>
#include <string.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>


//measure of latency 
void getlatency()
    {
     FILE *fp ;
     char var[90] ;
     char latency[50];
     char latency_cmdline [100];
     char normalvalue[50];
     // ping -c 10 8.8.4.4 | tail -1| awk '{print $4}' | cut -d / -f 2
     sprintf(latency_cmdline,"%s%s%s","ping -c 10 www.google.com | tail -1| awk '","{print $4}'","| cut -d / -f 2");
     fp=popen(latency_cmdline ,"r") ;
     while (fgets( var,sizeof(var),fp)!=NULL ) {
        sscanf(var,"%s",latency ) ; 
        if ( latency <= normalvalue )
           printf("%s is low latency!",latency);
           else 
           printf("%s is high latency!",latency);

        fclose(fp) ;
     }
     
    } 
    //measure of packet loss
    void getpacketloss()
    { 
        FILE *fp ;
        char var[50] ;
        char packetloss[50];
        char loss_cmdline[100];

        //ping -c 5 -q $host | grep -oP '\d+(?=% packet loss)'
        // sprintf(latency_cmdline,"%s%s%s","ping -c 10 www.google.com | tail -1| awk '","{print $4}'","| cut -d / -f 2");
        sprintf(loss_cmdline,"%s%s","ping -c 5 -q www.google.com| grep -oP '","\\d+(?=% packet loss )'" ) ;
        fp=popen(loss_cmdline, "r");
        while (fgets( var,sizeof(var),fp)!=NULL ) {
        sscanf(var,"%s",packetloss ) ; 
        printf("%s packetloss !",packetloss) ;
        if (packetloss> "3")
         printf("network is not performing well !!") ;

         fclose(fp) ;     
           }

    }



int main()
{
    getlatency();
    getpacketloss();
    return(0);
}
