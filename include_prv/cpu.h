#ifndef _CPU_H
#define _CPU_H

/* declaration des types publics de monModule */

  typedef struct {
    char* cpu_user; /*!< Time spent in user mode */
    char* cpu_nice; /*!< Time spent in user mode with low priority */
    char* cpu_system; /*!< Time spent in system mode */
    char* cpu_idle; /*!< Time spent in the idle task */
    char* cpu_iowait; /*!< Time waiting for I/O to complete */
    char* cpu_irq; /*!< Time servicing interrupts */
    char* cpu_guest_system; /*!< Time spent running a virtual CPU for guest systems */
    char* cpu_softirq; /*!< Time servicing softirqs */
    char* cpu_steal_time; /*!< Time spent in other operating systems when running in a virtualized environment */
   
} cpu_measurement_t;


 typedef struct {
    char* rank; /*!< Time spent in user mode */
    char* pid; /*!< Time spent in user mode with low priority */
    char* process_percentage; 
    char* cmd;/*!< Time spent in system mode */
    //char* cpu_idle; /*!< Time spent in the idle task */
    //char* cpu_iowait; /*!< Time waiting for I/O to complete */
    //char* cpu_irq; /*!< Time servicing interrupts */
    //char* cpu_guest_system; /*!< Time spent running a virtual CPU for guest systems */
    //char* cpu_softirq; /*!< Time servicing softirqs */
    //char* cpu_steal_time; /*!< Time spent in other operating systems when running in a virtualized environment */
   
}  cpup_measurement_t;


/* prototype des fonctions publiques de monModule */
void collect_cpu_values_general() ;
void collect_cpu_values_per_process() ;


 #endif