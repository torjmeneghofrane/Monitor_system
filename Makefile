#-*- MakeFi.gh	.
#target: dependencies 
#	action 
INCLDIR = include_priv

all :cpu 
cpu: src/modulecpu.c 
	$(CC) -I$(INCLDIR) -o $@ $^ -ljson-c -lmosquitto

clean : 
	rm -f cpu
